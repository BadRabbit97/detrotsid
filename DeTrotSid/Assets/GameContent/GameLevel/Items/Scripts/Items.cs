﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    //*****ITEMS SCRIPT*****
    //This script controls and manages the pick ups within the level

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Float variable (decimal numeric) - stores the distance of the area of effect
    //Distance the player canpick up an object
    public float areaOfEffect = 0.5f;

    //Float variable(decimal numeric) - stores and controls the amount of time the pop up has been up
    //public float popUpTime;
    //Float variable(decimal numeric) - stores and controls the assigned popup duration
    //public float popUpDuration;

    //Instance variable - Position variable - stores and referances the position of the pick up point
    public Transform pickUpPoint;

    //Instance variable - stores and controls what object the sphere should check for
    public LayerMask pickUpLayer;

    //Instance variable - stores the information from another script
    private Inventory inventory;

    //Instance variable - stores the itemButtons
    public GameObject itemButton;

    //Game object varaible (stores object) - stores infor on  game object
    //public GameObject PopUpUI;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //Hides pop up
        //PopUpUI.SetActive(false);

        //Inventory equals the inventory component attached to the player
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    }

    //Update is called once per frame
    void Update()
    {
        //Calls Collect() function
        Collect();
    }

    //***FUNCTIONS***

    //This function allows the pick up to be moved to the inventory slot
    void Collect()
    {
        //Detects if the pick up is in range of the player 
        Collider[] collectPickUp = Physics.OverlapSphere(pickUpPoint.position, areaOfEffect, pickUpLayer);

        //Collect pick up
        foreach (Collider Player in collectPickUp)
        {
            //This makes the timer a timer and not a countdown
            //popUpTime += Time.deltaTime;

            /*//If the pop up time hasn't run out...
            if (popUpTime < popUpDuration)
            {
                //...shows pop-up
                PopUpUI.SetActive(true);
            }
            //However if the pop up time has run out...
            if (popUpTime > popUpDuration)
            {
                //Hides pop up
                PopUpUI.SetActive(false);
            }*/

            //If F button is pressed...
            if (Input.GetKeyDown(KeyCode.F))
            {
                //Check to see if there is an empty slot
                for (int i = 0; i < inventory.slots.Length; i++)
                {
                    //if there is a free slot...
                    if (inventory.isFull[i] == false)
                    {
                        //..item can be picked up and added to inventory

                        //State the inventory slot is now full
                        inventory.isFull[i] = true;

                        //Creates an instance of itemButton
                        Instantiate(itemButton, inventory.slots[i].transform, false);

                        //Destroys game object
                        Destroy(gameObject);

                        //Stops for loop
                        break;
                    }

                }
            }
        }
    }

    //Sets up area if effect visual
    void OnDrawGizmosSelected()
    {
        //Incase pick up point has not been asigned
        //If the pick up point is null...
        if (pickUpPoint == null)
            //...return before anything happens
            return;

        //Shows the area of effect boundries
        Gizmos.DrawWireSphere(pickUpPoint.position, areaOfEffect);
    }
}
