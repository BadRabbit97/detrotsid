﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //*****PLAYER SCRIPT*****
    //This script controls the players health.

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //Integer variable (numeric) - stores player max amount of health
    public int maxHealth = 100;
    //Integer variable (numeric) - stores the players current health
    public int currentHealth;

    //Float variable (decimal numeric) - stores the distance of attack
    public float areaOfEffect = 0.5f;

    //Instance variable - creates referance to Player_HealthBar script
    public Player_HealthBar healthBar;

    //Instance variable - Position variable - stores and referances the position of the attack point
    public Transform reachPoint;

    //Instance variable - stores and controls what object the sphere should check for
    public LayerMask enemyLayer;

    //AudioClip referance -stores the information from the assigned audio
    public AudioClip damageAudio;
    //AudioClip referance -stores the information from the assigned audio
    public AudioClip playerAttack;
    //AudioSource referance -stores the information from the assigned audio
    public AudioSource playerAudio;

    //Referance variable - stores the information from another script
    HealthText healthText;

    //Referance variable - stores the information from another script
    //CountdownText countdownText;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //healthText equals the information from the script HealthText
        healthText = GetComponent<HealthText> ();

        //Sets the current health to equal the max health
        currentHealth = maxHealth;
        //Allows for the player health value to move between scripts
        healthBar.SetMaxHealth(maxHealth);

        //Calls the SetHealthAmount function - sets the player health text to the starting player health amount
        SetHealthAmount();
    }

    // Update is called once per frame
    void Update()
    {
        //If spacebar pressed...
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //...damage enemy health
            Attack();
        }
    }

    //***FUNCTIONS***

    //Public function - can be called from other scripts
    //Makes the player take damage
    public void TakeDamage(int damage)
    {
        //Subtracts the damage from the current health
        currentHealth -= damage;

        //Updates the health bar to the current health
        healthBar.SetHealth(currentHealth);

        //Calls the SetHealthAmount function - updates the players health text to the current health
        SetHealthAmount();

        //Play audio
        playerAudio.PlayOneShot(damageAudio, 0.2f);

        //If enemies health is lower or equal to 0...
        if (currentHealth <= 0)
        {
            //...will lock 0 in health
            currentHealth = 0;

            //...calls Die function
            PlayerDeath();
        }
    }

    //This function kills the enemy at starts the next scene
    void PlayerDeath()
    {
        //Loads the bad scene from scene manager
        //Adds fade to scene change
        fadeLevel.GetComponent<LevelChanger>().FadeToLevel(16);
    }

    //Sets health text to player amount when called.
    void SetHealthAmount()
    {
        //Sets health text to player amount
        healthText.healthAmount.text = currentHealth.ToString();
    }

    //This function detects and inflicts damage to the enemy
    void Attack()
    {
        //Detects if the enemy is in range of attack
        Collider[] hitEnemy = Physics.OverlapSphere(reachPoint.position, areaOfEffect, enemyLayer);

        //Damages the enemy
        foreach(Collider enemy in hitEnemy)
        {
            //Links to Hazard script to damage health
            enemy.GetComponent<Hazard>().TakeDamage(2);

            //Play audio
            playerAudio.PlayOneShot(playerAttack, 0.5f);
        }
    }

    //Sets up
    void OnDrawGizmosSelected()
    {
        //Incase attack point has not been asigned
        //If the attack point is null...
        if (reachPoint == null)
            //...return before anything happens
            return;

        //Shows the attack point boundries
        Gizmos.DrawWireSphere(reachPoint.position, areaOfEffect);
    }
}