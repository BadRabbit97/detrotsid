﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //*****PLAYER MOVEMENT SCRIPT*****
    //This script controls the players movement

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Float variable(decimal numeric) - stores and controls the walk speed of the players movement
    public float walkSpeed = 4f;
    //Float variable(decimal numeric) - stores and controls the sprint speed of the players movement
    public float sprintSpeed = 4f;
    //Float variable(decimal numeric) - stores and controls the sprint crouch of the players movement
    public float crouchSpeed = 4f;
    //Float variable(decimal numeric) - stores and controls the gravity applied to the player
    public float gravity = -9.81f;
    //Float variable(decimal numeric) - radius of sphere that checks collision
    public float groundDistance = 0.1f;

    //Boolean variable (true or false) - checks to see if player object is grounded
    bool isGrounded;

    //AudioClip referance -stores the information from the assigned audio
    public AudioClip crouchAudio;
    //AudioClip referance -stores the information from the assigned audio
    public AudioClip walkAudio;
    //AudioClip referance -stores the information from the assigned audio
    public AudioClip sprintAudio;
    //AudioClip referance -stores the information from the assigned audio
    public AudioSource movementAudio;

    //Instance variable - stores and referances the object CharacterController, the motor that moves the player
    public CharacterController controller;
    //Instance variable - Position variable - stores and referances the position of the ground check
    public Transform groundCheck;
    //Instance variable - stores and controls what object the sphere should check for
    public LayerMask groundMask;
    //Instance variable - represents 3D vectors and ponits (x, y, z) - stores the current velocity
    Vector3 velocity;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //Finds the CharacterController on the player
        controller = gameObject.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        //Calls the Move function
        Move();
    }

    //***FUNCTIONS***

    //Controls player movement
    void Move()
    {
        //*****DECLARED VARIABLES*****
        //Float variable(decimal numeric) - gathers the x input of player (pre-programned axis)
        float x = Input.GetAxis("Horizontal");
        //Float variable(decimal numeric) - gather the z input of player (pre-programned axis)
        float z = Input.GetAxis("Vertical");

        //Check to see if player object is grounded
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        //If playerobject is grounded AND velocity is less than 0...
        if (isGrounded && velocity.y < 0)
        {
            //...resets velocity
            velocity.y = -2f;
        }

        //Moves the player object in the direction it is facing
        //Vector 3 - represents 3D vectors and ponits (x, y, z)
        Vector3 move = transform.right * x + transform.forward * z;

        //If left shift is pressed...
        if (Input.GetKey(KeyCode.LeftShift))
        {
            //...the player sprints
            //Time.deltaTime - frame rate independant
            controller.Move(move * sprintSpeed * Time.deltaTime);

            //Contorls the hight of the player
            controller.height = 1.4f;

            //If the audio is not playing...
            if (controller.velocity.magnitude > 5f && GetComponent<AudioSource>().isPlaying == false)
            {
                //...play audio
                movementAudio.PlayOneShot(sprintAudio, 0.02f);
            }
        }
        //...or if left control is pressed..
        else if (Input.GetKey(KeyCode.LeftControl))
        {
            //...the player crouches
            //Time.deltaTime - frame rate independant
            controller.Move(move * crouchSpeed * Time.deltaTime);

            //Contorls the hight of the player
            //Makes the player crouch
            controller.height = 0.4f;

            //If the audio is not playing...
            if (controller.velocity.magnitude > 0.5f && GetComponent<AudioSource>().isPlaying == false)
            {
                //...play audio
                movementAudio.PlayOneShot(crouchAudio, 0.02f);
            }
        }
        //...or defaul...
        else
        {
            //...the player walks
            //Time.deltaTime - frame rate independant
            controller.Move(move * walkSpeed * Time.deltaTime);

            //Contorls the height of the player
            controller.height = 1.4f;

            //If the audio is not playing...
            if (controller.velocity.magnitude > 2f && GetComponent<AudioSource>().isPlaying == false)
            {
                //...play audio
                movementAudio.PlayOneShot(walkAudio, 0.02f);
            }
        }

        //Increases velocity the longer the player falls
        velocity.y += gravity * Time.deltaTime;

        //Moves the player object with gravity - free fall
        controller.Move(velocity * Time.deltaTime);
    }
}
