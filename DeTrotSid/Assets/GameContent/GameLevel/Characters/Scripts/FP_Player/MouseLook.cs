﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    //*****MOUSE LOOK SCRIPT*****
    //This script controls the players ability to look around.

    //***DECLARED VARIABLES***

    //Float variable(decimal numeric) - stores and controls the rotation around the x-axis
    float xRotation = 0f;
    //Public - can be changed wihin Unity
    //Float variable(decimal numeric) - stores and controls the speed of the mouse sensitivity
    public float mouseSensitivity = 100f;

    //Instance variable - stores and controls the position of the first person players body
    public Transform playerBody;


    //***UPDATES***
    // Start is called before the first frame update
    void Start()
    {
        //Hide and locks the cursor to the center of the screen
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        //*****DECLARED VARIABLES*****
        //Float variable(decimal numeric) - gather the mouse x input (pre-programned axis)
        //The gathered input is mutliplied by mouse sensitivity, controls the speed of the player turning
        //Time.deltaTime - frame rate independant, stops quicker turning with higher frame rates
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        //Float variable(decimal numeric) - gather the mouse y input (pre-programned axis)
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        //Every frame rate decreases the x rotation
        xRotation -= mouseY;
        //Stops over rotation
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        //Applies the x rotation
        //Quarternion - controls rotations
        //Allows for x rotation, while allowing for a Clamp
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        //Allows the camera movement to move the first person player body object
        playerBody.Rotate(Vector3.up * mouseX);
    }
}
