﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Hazard : MonoBehaviour
{
    //*****HAZARD SCRIPT*****
    //This script controls the enemy hazard npc's damage and health.

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //Integer variable (numeric) - stores the enemies max amount of health
    public int hzdMaxHealth = 100;
    //Integer variable (numeric) - stores the enemies current health
    public int hzdCurrentHealth;

    //Integer variable (numeric) - stores the enemie damage on player
    public int damage = 10;

    //Vector 3 referance - stores the position of the object 
    private Vector3 startPoint;

    //Instance variable - creates referance to Player_HealthBar script
    public Hazard_HealthBar healthBar;

    //Referance variable - stores the information from another script
    Hzd_HealthText hzdHealthText;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //Sets start point to the object transform
        startPoint = transform.position;

        //healthText equals the information from the script HealthText
        hzdHealthText = GetComponent<Hzd_HealthText>();

        //Sets the current health to equal the max health
        hzdCurrentHealth = hzdMaxHealth;
        //Allows for the player health value to move between scripts
        healthBar.SetMaxHealth(hzdMaxHealth);

        //Calls the SetHealthAmount function - sets the player health text to the starting player health amount
        SetHealthAmount();
    }

    // Update is called once per frame
    void Update()
    {
        //Calls the SetHealthAmount function - sets the player health text to the enemies health amount
        SetHealthAmount();
    }

    //***FUNCTIONS***

    //Makes the player take damage
    public void TakeDamage(int damage)
    {
        //Subtracts the damage from the current health
        hzdCurrentHealth -= damage;

        //Updates the health bar to the current health
        healthBar.SetHealth(hzdCurrentHealth);

        //If enemies health is lower or equal to 0...
        if (hzdCurrentHealth <= 0)
        {
            //...will lock 0 in health
            hzdCurrentHealth = 0;

            //...calls Die function
            Die();
        }
    }

    //This function kills the enemy at starts the next scene
    void Die()
    {
        //Loads the bad scene from scene manager
        //Adds fade to scene change
        fadeLevel.GetComponent<LevelChanger>().FadeToLevel(8);
    }

    //Sets health text to player amount when called.
    void SetHealthAmount()
    {
        //Sets health text to enemy amount
        hzdHealthText.healthAmount.text = hzdCurrentHealth.ToString();
    }

    //Built-in unity function for collision
    //On object collision, this function will be called 
    void OnTriggerEnter(Collider collisionData)
    {
        //When object collides with player object, player will take damage
        collisionData.gameObject.GetComponent<Player>().TakeDamage(damage);

        //Place enemy at the generated co-ordinates
        transform.position = startPoint;
    }
}