﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HazardMovement : MonoBehaviour
{
    //*****HAZARD MOVEMENT SCRIPT*****
    //This script creates and controls the different state for the enemy ai

    //***DECLAIRED VARAIBLES***

    //SET UP
    //Publlic - can be seen and edited in unity editor
    //Reference to the nav mesh agent in use
    public NavMeshAgent agent;
    //Transform - this stores the transfomr from the player object
    public Transform player;
    //LayerMask - finds the object with that layer
    public LayerMask whatIsGround;
    public LayerMask whatIsPlayer;

    //PATROLING
    //Vector3  - controls and contains the position of an object
    public Vector3 walkPoint;
    //Bool variable (true or false) - controls if the walk point has been set
    bool walkPointSet;
    //Float variable (decimal numeric) - controls how far the ai should walk
    public float walkPointRange;

    //ATTACKING
    //Float variable (decimal numeric) - controls how often the ai attacks
    public float timeBetweenAttacks;
    //Bool varaible (true or false) - controls if the ai has already attacked the player
    bool alreadyAttacked;

    //STATES
    //Float variable (decimal numeric) - controls rande of enemy sight and attack range
    public float sightRange;
    public float attackRange;
    //Bool variable (true or false) - checks to see if player is in range
    public bool playerInSightRange;
    public bool playerInAttackRange;

    //***UPDATES***

    // Start is called before the first frame update
    void Start()
    {
        //Search for player object
        player = GameObject.Find("FP_Player").transform;
        //Assign Nav Mesh agent
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        //Check if the player is in sight range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        //Check if the player is in attack range
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        //If the player is not in the sight range or the attack range...
        if (!playerInSightRange && !playerInAttackRange)
        {
            //...the enemy should patrol
            //Calls Patrolling() function
            Patrolling();
        }
        //If the player is in the sight range but not in the attack range...
        if (playerInSightRange && !playerInAttackRange)
        {
            //...the enemy should chase the player
            //Calls ChasePlayer() function
            ChasePlayer();
        }
    }

    //***FUNCTIONS***

    //This function controls the patrolling state of the ai
    private void Patrolling()
    {
        //If the search point is not set...
        if (!walkPointSet)
        {
            //...serach for walk point
            SearchWalkPoint();
        }
        //If a walk point is set...
        if (walkPointSet)
        {
            //...set the wlak point to the agent
            //Enemy walks to new position
            agent.SetDestination(walkPoint);
        }

        //Calculates distance to walk point
        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walk point reached
        //If the distance is less than 1...
        if (distanceToWalkPoint.magnitude < 1f)
        {
            //...walk point has been reached
            walkPointSet = false;
        }
    }

    //This function searches for a walk point
    private void SearchWalkPoint()
    {
        //Float variable (decimal numeric) - calculates random point in range
        //Returns a random value
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        //Sets a new walk point for the enemy
        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        //If walk point is in the map...
        if(Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
        {
            //...new walk point can occur
            //Set the new walk point to true
            walkPointSet = true;
        }
    }

    //This function controls the chase state of the ai
    private void ChasePlayer()
    {
        //Enemy chases player
        agent.SetDestination(player.position);
    }
}
