﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorMovement : MonoBehaviour
{
    //*****DOOR MOVEMENT SCRIPT*****
    //This script controls the interaction of the doors as well as their movement

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Float variable (decimal numeric) - stores the distance of the area of effect
    //Distance the player canpick up an object
    public float areaOfEffect = 1f;

    //Float variable(decimal numeric) - stores and controls the timer for how long the inventory has been open
    public float keyPressTime;
    //Float variable(decimal numeric) - stores and controls what time the timer should start at
    public float startTime;
    //Float variable(decimal numeric) - stores and controls the assigned wait time
    public float waitTime;

    //Boolean varaible (true or false) - controls when the inventory timer starts
    bool timerActive = false;

    //Animation variable - stores the animation on the game object
    Animator doorAnim;

    //Instance variable - Position variable - stores and referances the position of the pick up point
    public Transform pickUpPoint;

    //Instance variable - stores and controls what object the sphere should check for
    public LayerMask pickUpLayer;

    //AudioClip referance -stores the information from the assigned audio
    public AudioClip doorOpen;
    //AudioClip referance -stores the information from the assigned audio
    public AudioClip doorClose;
    //AudioClip referance -stores the information from the assigned audio
    public AudioSource movementAudio;

    //***UPDATES***

    // Start is called before the first frame update
    void Start()
    {
        //Finds the animator stored on the game object
        doorAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //If the timer is active...
        //Door is open
        if (timerActive == true)
        {
            //...timer will go up (addition rather than subtraction)
            //This makes the timer a timer and not a countdown
            keyPressTime += Time.deltaTime;
        }

        //If the timer is not active...
        //Door is closed
        if (timerActive == false)
        {
            //...the timer will reset
            keyPressTime = startTime;
        }

        //Calls DoorMove() function
        DoorMove();
    }

    //***FUNCTIONS***

    //This function will open and close the door
    void DoorMove()
    {
        //Detects if the pick up is in range of the player 
        Collider[] collectPickUp = Physics.OverlapSphere(pickUpPoint.position, areaOfEffect, pickUpLayer);
        //Collect pick up
        foreach (Collider Player in collectPickUp)
        {
            //If F button is pressed...
            if (Input.GetKeyDown(KeyCode.F))
            {
                //Activates the timer when the door is open
                timerActive = !timerActive;

                //Triggers open door animation
                doorAnim.SetTrigger("OpenDoor");

                //Play audio
                movementAudio.PlayOneShot(doorOpen, 0.2f);
            }
            //However if F is pressed and enough time has passed...
            else if (Input.GetKeyDown(KeyCode.F) && keyPressTime > waitTime)
            {
                Debug.Log("Here");
                //Triggers close door animation
                doorAnim.SetTrigger("CloseDoor");

                //Play audio
                movementAudio.PlayOneShot(doorClose, 0.2f);
            }
        }
    }

    //Sets up area if effect visual
    void OnDrawGizmosSelected()
    {
        //Incase pick up point has not been asigned
        //If the pick up point is null...
        if (pickUpPoint == null)
            //...return before anything happens
            return;

        //Shows the area of effect boundries
        Gizmos.DrawWireSphere(pickUpPoint.position, areaOfEffect);
    }
}
