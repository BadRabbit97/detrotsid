﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_HealthBar : MonoBehaviour
{
    //*****PLAYER HEALTH BAR SCRIPT*****
    //This script controls the players health bar.

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Instance variable - stores and controls the health bar slider
    public Slider slider;
    //Instance variable - stores the gradient of the fill on the health bar
    public Gradient gradient;
    //Instance variable - stores the health bar fill
    public Image fill;

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //Sets the max amount of health the player has
    public void SetMaxHealth(int health)
    {
        //Sets the sliders max value equal to the amount of health the player
        slider.maxValue = health;
        //Makes slider start at the maximum amount of health
        slider.value = health;

        //Starts the health bar fill colour on green
        fill.color = gradient.Evaluate(1f);
    }

    //Sets the health on the slider - feeds the integer of the players health
    public void SetHealth(int health)
    {
        //Sets the slider variable equal to the amount of health the player has
        slider.value = health;

        //Changes the fill colour to the amount of health left
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
