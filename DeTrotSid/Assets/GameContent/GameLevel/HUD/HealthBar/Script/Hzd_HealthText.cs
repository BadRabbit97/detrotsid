﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hzd_HealthText : MonoBehaviour
{
    //*****HAZARD HEALTH TEXT SCRIPT*****
    //This script will moniter and update the hazards health text amount.

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Instance variable - referances and stores the health amount text that is suppose to change in the HUD
    public Text healthAmount;
}
