﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class WeaponEquip : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    //*****WEAPON EQUIP SCRIPT*****
    //This script controls and manages the weapon equiped slot to store the weapon that is currently in use

    //***DECLARED VARIABLES***

    //Instance variable - stores the item button sprite
    public GameObject sprite;

    //Instance variable - stores the slot game object
    public GameObject[] WeaponSlot;

    //Instance variable - stores the information from another script
    private Inventory inventory;

    //Instance variable - stores the information from another script
    

    //Boolean varaible (true or false) - stors an inventory menu game object
    public GameObject inventoryMenuUI;

    //Boolean varaible (true or false) - stores whither inventory slot is full
    public bool[] isFull;

    //***UPDATES***

    // Start is called before the first frame update
    void Start()
    {
        //Inventory equals the inventory component attached to the player game object
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();

        //Sprite equals the items component attached to the item game object
        //GameObject item = GameObject.FindGameObjectWithTag("Item");
        //sprite = item.GetComponent<Items>().itemButton;

        //
        
    }

    // Update is called once per frame
    void Update()
    {
    
    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function is called when the mouse button is clicked
    public void OnPointerClick(PointerEventData eventData)
    {
        //
        if (EventSystem.current.IsPointerOverGameObject())
        {
            //
            //if(gameObject.tag == ("Weapon"))
            //{
                //...call EquipWeapon function
                EquipWeapon();
            //}
        }
    }

    //
    public void OnPointerEnter(PointerEventData eventData)
    {
        
    }

    //
    public void OnPointerExit(PointerEventData eventData)
    {
        //
        Debug.Log("Normal");
    }

    //This function equips a weapon to the player
    void EquipWeapon()
    {
        //Check to see if there is an empty slot
        for (int i = 0; i < WeaponSlot.Length; i++)
        {
            //If slot is empty...
            if (isFull[i] == false)
            {
                //...Add object to weapon equipped

                //State the weapon slot is now full
                isFull[i] = true;

                //Creates an instance of itemButton
                Instantiate(sprite, WeaponSlot[i].transform, false);

                //...And add the weapons damage bonus to the player
            }
            //Else if the slot is full
            else if (isFull[i] == true)
            {
                //Remove currrent object

                //Remove the weapons damage bonus to the player

                //Creates the new instance of itemButton

                //And add the weapons damage bonus to the player

            }
        }
    }
}
