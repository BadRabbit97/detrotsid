﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    //*****INVENTORY SCRIPT*****
    //This script controls and manages the inventory

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Float variable(decimal numeric) - stores and controls the assigned wait time
    public float waitTime;
    //Float variable(decimal numeric) - stores and controls the timer for how long the inventory has been open
    public float keyPressTime;
    //Float variable(decimal numeric) - stores and controls what time the timer should start at
    public float startTime;

    //Boolean varaible (true or false) - stores whither inventory slot is full
    public bool[] isFull;

    //Boolean varaible (true or false) - controls when the inventory timer starts
    bool timerActive = false;

    //Game object varaible (stores object) - stores infor on  game object
    public GameObject inventoryMenuUI;
    //Game object varaible (stores object) - stores infor on  game object
    public GameObject HUDMenuUI;
    //Game object varaible (stores object) - stores infor on  game object
    public GameObject weaponEquipUI;

    //Instance variable - stores the amount of slots
    public GameObject[] slots;

    //Instance variable - stores the information from another script
    public MouseLook mouseLook;

    //***UPDATES***

    // Start is called before the first frame update
    void Start()
    {
        //Hides pause menu during gameplay
        inventoryMenuUI.SetActive(false);

        //Shows HUD during gameplay
        HUDMenuUI.SetActive(true);

        //Shows Weapon HUD during gameplay
        weaponEquipUI.SetActive(true);

        //MouseLook equals the mouse look component attached to the Main Camera game object
        mouseLook = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MouseLook>();
    }

    // Update is called once per frame
    void Update()
    {
        //If the timer is active...
        //Inventory is open
        if (timerActive == true)
        {
            //...timer will go up (addition rather than subtraction)
            //This makes the timer a timer and not a countdown
            keyPressTime += Time.deltaTime;
        }

        //If the timer is not active...
        //Inventory is closed
        if (timerActive == false)
        {
            //...the timer will reset
            keyPressTime = startTime;
        }

        //If I is pressed...
        if (Input.GetKeyDown(KeyCode.I))
        {
            //...opens inventory
            //Calls OpenInventory() function
            OpenInventory();
        }
    }

    //***FUNCTIONS***

    //This function opens the inventory
    void OpenInventory()
    {
        //Activates the timer when the inventory is open
        timerActive = !timerActive;
        //Shows inventory
        inventoryMenuUI.SetActive(!inventoryMenuUI.activeSelf);

        //Hides HUD during gameplay
        HUDMenuUI.SetActive(!HUDMenuUI.activeSelf);

        //Shows Weapon HUD during gameplay
        weaponEquipUI.SetActive(true);

        //Sets cursor to the middle of the screen
        //Accesses the cursor API, look at the lock state and set it to locked
        Cursor.lockState = CursorLockMode.None;
        //Shows cursor on inventory page
        Cursor.visible = true;

        //Turns camera movement off
        mouseLook.enabled = false;

        //If I is pressed...
        if (Input.GetKeyDown(KeyCode.I) & keyPressTime > waitTime)
        {

            //Hides inventory
            inventoryMenuUI.SetActive(false);

            //...hide cursor
            Cursor.visible = false;

            //...turns camera movement on
            mouseLook.enabled = true;
        }
    }
}
