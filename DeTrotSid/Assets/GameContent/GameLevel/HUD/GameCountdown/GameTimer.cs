﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameTimer : MonoBehaviour
{
    //*****GAME TIMER SCRIPT*****
    //This script will create and control the minute:second timer for the game level

    //***DECLAIRED VARIABLE***

    //Public - can be seen and edited within unity engine
    //Float variable (decimal numeric) - stores and controls the game timer
    public float timeValue = 90;

    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //Text object - text display of the countdown
    public Text timerText;

    //***UPDATES***

    // Update is called once per frame
    void Update()
    {
        //If the time is greater than 0...
        if (timeValue > 0)
        {
            //...makes the timer countdown
            timeValue -= Time.deltaTime;
        }
        else
        {
            //Good ending scene load
            //Loads the game level from scene manager
            //Adds fade to scene change
            fadeLevel.GetComponent<LevelChanger>().FadeToLevel(5);
        }

        //Calls the DisplayTime() function
        DisplayTime(timeValue);
    }

    //***FUNCTIONS***

    //This function will display the timer in the game
    void DisplayTime(float timeToDisplay)
    {
        //If timer drops below 0...
        if(timeToDisplay < 0)
        {
            //...will lock 0 in timer
            timeToDisplay = 0;
        }
        //Display 1 second left on timer at halta second
        //However if timer is still above 0...
        else if(timeToDisplay > 0)
        {
            //...display 1 secound left
            timeToDisplay += 1;
        }

        //Float variable (decimal numeric) - stores the minutes value
        //FloorToInt - rounds the decimal value down
        //Divides the time value by 60 (60 seconds in a minute)
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        //Float variable (decimal numeric) - stores the secounds value
        //Calculates how many seconds are left after the devision
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        //Sets text value to a new string that uses minutes and seconds value
        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
