﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MM_Background : MonoBehaviour
{
    //*****MAIN MENU BACKGROUND SCRIPT*****
    //This script controls and manages the main menu background

    //***UPDATES***

    // Update is called once per frame
    void Update()
    {
        //This rotates background 3 anti-clockwise
        transform.Rotate(new Vector3 (0f, 0f, 5f) * Time.deltaTime);
    }
}
