﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Credit_btn : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //*****CREDIT BUTTON SCRIPT*****
    //This script controls the credit button functions

    //***DECLARED VARIABLES***

    //Public - can be seen and edited in unity editor
    //AudioSource referance - stores audio source information
    public AudioSource buttonClick;

    //Vector variable - stores an object size
    Vector3 buttonScale;

    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //Sets and stores buttonScale to equal to the size off the object
        buttonScale = transform.localScale;
    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function controls what scene the credit button goes to when clicked
    public void CreditButton()
    {
        //Plays audio
        buttonClick.Play();

        //Loads the credit scene from scene manager
        //Adds fade to scene change
        fadeLevel.GetComponent<LevelChanger>().FadeToLevel(4);
    }

    //This function is called whenever the mouse cursor enters the position of the object
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Increases size of object
        transform.localScale = new Vector3(0.6f, 0.6f, 1f);
    }

    //This function is called whenever the mouse cursor leaves the position of the object
    public void OnPointerExit(PointerEventData eventData)
    {
        //Returns the object size to the original size
        transform.localScale = buttonScale;
    }
}
