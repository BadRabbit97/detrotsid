﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Play_btn : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //*****PLAY BUTTON SCRIPT*****
    //This script controls the play button functions

    //***DECLARED VARIABLES***

    //Vector variable - stores an object size
    Vector3 buttonScale;

    //Public - can be seen and edited in unity editor
    //AudioSource referance - stores audio source information
    public AudioSource buttonClick;

    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //Sets and stores buttonScale to equal to the size off the object
        buttonScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function controls what scene the play button goes to when clicked
    public void PlayGame()
    {
        //Plays audio
        buttonClick.Play();

        //Loads the portal scene from scene manager
        //Adds fade to scene change
        fadeLevel.GetComponent<LevelChanger>().FadeToLevel(13);
    }

    //This function is called whenever the mouse cursor enters the position of the object
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Increases size of object
        transform.localScale = new Vector3(0.6f, 0.6f, 1f);
    }

    //This function is called whenever the mouse cursor leaves the position of the object
    public void OnPointerExit(PointerEventData eventData)
    {
        //Returns the object size to the original size
        transform.localScale = buttonScale;
    }
}
