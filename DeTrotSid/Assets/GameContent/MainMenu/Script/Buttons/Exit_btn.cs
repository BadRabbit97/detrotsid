﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Exit_btn : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //*****EXIT BUTTON SCRIPT*****
    //This script controls the exit button functions.

    //***DECLARED VARIABLES***

    //Public - can be seen and edited in unity editor
    //AudioSource referance - stores audio source information
    public AudioSource buttonClick;

    //Vector variable - stores an object size
    Vector3 buttonScale;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //Sets and stores buttonScale to equal to the size off the object
        buttonScale = transform.localScale;
    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function controls what scene the exit button goes to when clicked
    public void QuitGame()
    {
        //Plays audio
        buttonClick.Play();

        //Informs button is working
        Debug.Log("Quit!");
        //Quits the game
        Application.Quit();
    }

    //This function is called whenever the mouse cursor enters the position of the object
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Increases size of object
        transform.localScale = new Vector3(0.6f, 0.6f, 1f);
    }

    //This function is called whenever the mouse cursor leaves the position of the object
    public void OnPointerExit(PointerEventData eventData)
    {
        //Returns the object size to the original size
        transform.localScale = buttonScale;
    }
}
