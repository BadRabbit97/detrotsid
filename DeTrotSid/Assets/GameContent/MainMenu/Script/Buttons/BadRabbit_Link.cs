﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BadRabbit_Link : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //*****BAD RABBIT PRODUCTIONS LINK BUTTON SCRIPT*****
    //This script controls the BadRabbitProductions link button functions

    //***DECLARED VARIABLES***

    //Vector variable (Position) - stores an object size
    Vector3 buttonScale;

    //Public - can be changed wihin Unity
    //Image variable stores the image of a selected game object
    public Image imageComponent;

    //Sprite variable - stores a sprite image
    public Sprite firstSprite;
    //Sprite variable - stores a sprite image
    public Sprite secondSprite;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //Sets and stores buttonScale to equal to the size off the object
        buttonScale = transform.localScale;

    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function sets up the website link of a button
    public void OpenLink()
    {
        //Allows link to website
        Application.ExternalEval("window.open(\"https://mailchi.mp/797ebd267f6c/bad-rabbit-productions\")");
    }

    //This function is called whenever the mouse cursor enters the position of the object
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Increases size of object
        transform.localScale = new Vector3(1.3f, 1.3f, 1f);

        //Changes the image of the button
        imageComponent.sprite = firstSprite;
    }

    //This function is called whenever the mouse cursor leaves the position of the object
    public void OnPointerExit(PointerEventData eventData)
    {
        //Returns the object size to the original size
        transform.localScale = buttonScale;

        ////Returns the original image of the button
        imageComponent.sprite = secondSprite;
    }
}
