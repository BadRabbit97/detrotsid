﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //*****MAIN MENU SCRIPT*****
    //This script controls the main functions od the main menu

    //***UPDATES***

    // Start is called before the first frame update
    void Start()
    {
        //Shows cursor in main menu
        Cursor.visible = true;
    }
}