﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    //*****LEVEL CHANGER SCRIPT*****
    //This scripts controls the fade in and fade out transitions

    //***DECLAIRED VARIABLES***

    //Public - can be seen and edited in unity editor
    //Animator referance - stores the animation

    public Animator anim;

    //Integer varable (numeric) - stores what scene should load next
    private int levelToLoad;

    //***UPDATES***

    // Update is called once per frame
    void Update()
    {
        
    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function controls the fade transition
    public void FadeToLevel (int levelIndex)
    {
        //Asiigns number to the assign level in scene manager
        levelToLoad = levelIndex;
        //Starts animation
        anim.SetTrigger("FadeOut");
    }

    //Fade to the next scene
    public void OnFadeComplete()
    {
        //Loads the assigned level
        SceneManager.LoadScene(levelToLoad);
    }
}
