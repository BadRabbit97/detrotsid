﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartUp : MonoBehaviour
{
    //*****START UP SCRIPT*****
    //This script controls the start up screen before the main screen

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //Image referance - stores the image of a selected game object
    public Image imageComponent;

    //Sprite referance - stores a sprite image
    public Sprite originalSprite;
    //Sprite referance - stores a sprite image
    public Sprite newSprite;

    //Float variable(decimal numeric) - stores and controls the assigned wait time
    public float logoWaitTime;
    //Float variable(decimal numeric) - stores and controls how long the logo flips for
    public float logoFlipTime;
    //Float variable(decimal numeric) - stores and controls the timer for how long the scene has been open
    public float sceneTime;

    //Float variable(decimal numeric) - stores and controls the rotation speed
    public float rotationSpeed;
    //Float variable(decimal numeric) - stores and controls howfast the rotation speed decreases
    public float rotationSpeedDecrease;

    //***UPDATES***

    // Start is called before the first frame update
    void Start()
    {
        //Sets the image of the logo
        imageComponent.sprite = originalSprite;
    }

    // Update is called once per frame
    void Update()
    {
        //Timer will start
        //Timer will go up (addition rather than subtraction)
        //This makes the timer a timer and not a countdown
        sceneTime += Time.deltaTime;

        //Calls the LogoFlip function
        LogoFlip();

        //Calls the MainMenu() function
        MainMenu();
    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function will control the change of the logo
    public void LogoFlip()
    {
        //If enough time has passed since the scene started
        //AND the logo hasn't flipped yet
        if (sceneTime > logoWaitTime & sceneTime < logoFlipTime)
        {
            //...rotate the image on the z-axis
            transform.Rotate(new Vector3(0f, rotationSpeed, 0f) * Time.deltaTime);

            //Changes the image of the button
            imageComponent.sprite = newSprite;
        }
        //Else if the logo has flipped for a certain amount of time...
        else if (sceneTime > logoFlipTime)
        {
            //Calls StopFlip() function
            StopFlip();
        }  
    }

    //This function decreases logo flip until it stops.
    public void StopFlip()
    {
        //If the rotation speed is still greater than 0..
        if (rotationSpeed > 0)
        {
            //...the rotation speed will start to decrease
            rotationSpeed -= rotationSpeedDecrease * Time.deltaTime;
            //...rotate the image on the z-axis with the new speed
            transform.rotation = Quaternion.Euler(0, rotationSpeed, 0);
        }
        //However if the rotation speed is less than 0..
        else if (rotationSpeed <= 0)
        {
            //..AND if the y rotation position does not equal 0
            if (transform.position.y != 0)
            {
                //...stops the rotation of the logo to face the front
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }
        
    }

    //This function will control the automatic change to the main menu screen
    public void MainMenu()
    {
        //If scene time is more than 10 secounds...
        if (sceneTime >= 14f)
        {
            //...loads the portal scene
            //Adds fade to scene change
            fadeLevel.GetComponent<LevelChanger>().FadeToLevel(1);
        }
    }
}
