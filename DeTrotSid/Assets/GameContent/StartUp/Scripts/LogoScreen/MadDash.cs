﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MadDash : MonoBehaviour
{
    //*****MAD DASH SCRIPT*****
    //This script controls the object that dashes across the screen to star the logo rotation

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Float variable(decimal numeric) - stores and controls the timer for how long the scene has been open
    public float sceneTime;
    //Float variable(decimal numeric) - stores and controls the maximum speed of the object
    public float maxSpeed;


    //Vector variable (Position) - store and controls the target A
    public Vector3 pointA;


    //***UPDATES***

    // Update is called once per frame
    void Update()
    {
        //Timer will start
        //Timer will go up (addition rather than subtraction)
        //This makes the timer a timer and not a countdown
        sceneTime += Time.deltaTime;

        //Calls the MadDash() function
        MonsterDash();
    }

    //***FUNCTION***

    //Public - can be called from other scripts
    //This function moves the object from A to B
    public void MonsterDash()
    {
        //If scene time is equals 1.8 secounds
        if (sceneTime >= 8.4f)
        {
            //...moves object
            //Sets the speed of the object
            var speed = maxSpeed * Time.deltaTime;
            //Moves the object to point a
            transform.position = Vector3.MoveTowards(transform.position, pointA, speed);
        }
    }
}
