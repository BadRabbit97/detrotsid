﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BackInTime : MonoBehaviour
{
    //*****GAME TIMER SCRIPT*****
    //This script will create and control the minute:second timer for the game level

    //***DECLAIRED VARIABLE***

    //Public - can be seen and edited within unity engine
    //Float variable (decimal numeric) - stores player max amount of health
    public float maxTime;
    //Float variable (decimal numeric) - stores the players current health
    public float currentTime;
    //Float variable (decimal numeric) - store how long current year should be displayed
    public float pause;

    //Instance variable - referances and stores the text attached to object
    public Text timeText;

    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //***UPDATES***

    //Start is called before the first frame update
    void Start()
    {
        //Sets the current health to equal the max health
        currentTime = maxTime;
    }

    // Update is called once per frame
    void Update()
    {
        //Calls the RewindTime() function
        StartCoroutine(RewindTime(currentTime));
    }

    //***FUNCTIONS***

    //Sets time text to current
    //IEnumerator - allows iteration threw a list
    IEnumerator RewindTime(float timeToDisplay)
    {
        //if current time is greater than 2451...
        if (currentTime > 2461)
        {
            //Sets delay next code line
            yield return new WaitForSeconds(pause);

            //Begins a countdown
            currentTime -= Time.deltaTime;
        }
        else
        {
            //...loads the portal scene
            //Adds fade to scene change
            fadeLevel.GetComponent<LevelChanger>().FadeToLevel(2);
        }

        //Float variable (decimal numeric) - stores the minutes value
        //FloorToInt - rounds the decimal value down
        //Divides the time value by 60 (60 seconds in a minute)
        float year = Mathf.CeilToInt(timeToDisplay);

        //Sets text value to a new string that uses minutes and seconds value
        timeText.text = "YEAR " + string.Format("{0:00}",  year);
    }
}
