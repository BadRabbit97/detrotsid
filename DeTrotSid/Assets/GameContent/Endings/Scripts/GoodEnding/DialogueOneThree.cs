﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DialogueOneThree : MonoBehaviour
{
    //***DIALOGUE ONE THREE SCRIPT*****
    //This script controls the how long the screen is displayed before the next scene

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //Float variable(decimal numeric) - stores and controls how long the text is displayed
    public float textTime;
    //Float variable(decimal numeric) - stores and controls the timer for how long the scene has been open
    public float sceneTime;

    //***UPDATES***

    // Update is called once per frame
    void Update()
    {
        //Timer will start
        //Timer will go up (addition rather than subtraction)
        //This makes the timer a timer and not a countdown
        sceneTime += Time.deltaTime;

        //Calls the function StoryTime()
        StoryTime();
    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function controls how long the screen is displayed
    public void StoryTime()
    {
        //If enough time has passed since the scene started...
        if (sceneTime > textTime)
        {
            //...load player goal scene from scene manager
            //Adds fade to scene change
            fadeLevel.GetComponent<LevelChanger>().FadeToLevel(15);
        }
    }
}
