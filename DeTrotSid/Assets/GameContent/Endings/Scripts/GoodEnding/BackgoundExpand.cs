﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgoundExpand : MonoBehaviour
{
    //*****BACKGROUND EXPAND SCRIPT*****
    //This script controls the expanding white background functions

    //***DECLARED VARIABLES***

    //Public - can be changed wihin Unity
    //Integer variable (numeric) - stores and controlds the start size of the image
    public int startSize = 3;
    //Integer variable (numeric) - stores and controls the minim size of the image
    public int minSize = 1;
    //Integer variable (numeric) - stores and controls the maximum size of the image
    public int maxSize = 6;

    //Float variable(decimal numeric) - stores and controls the scale speed of the image
    public float speed = 2.0f;

    //Float variable (decimal numeric) - store how long dialogue should be displayed
    public float backChange;

    //Vector variable (Position) - store and controls the target scale of the image
    private Vector3 targetScale;
    //Vector variable (Position) - stores and controls the base scale of the image
    private Vector3 baseScale;
    //Integer variable (numeric) - stores the current scale of the image
    private int currScale;

    //***UPDATES***

    // Start is called before the first frame update
    void Start()
    {
        //Sets base scale tot the original scale
        baseScale = transform.localScale;
        //Changes the original size to times the base size by start size
        transform.localScale = baseScale * startSize;
        //Sets current scale to the star size
        currScale = startSize;
        //Sets target scale to the base scale times the start size
        targetScale = baseScale * startSize;
    }

    // Update is called once per frame
    void Update()
    {
        //Changes the size of the image scale to to the speed of scale
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, speed * Time.deltaTime);

        //Calls the ChangeSize() function
        StartCoroutine(ChangeSize());
    }

    //***FUNCTIONS***

    //Public - can be called from other scripts
    //This function changes the size of the image
    //IEnumerator - allows iteration threw a list
    IEnumerator ChangeSize()
    {
        //Sets delay next code line
        yield return new WaitForSeconds(backChange);

        //Scales the image
        currScale++;
        //Chnages the current scale 
        currScale = Mathf.Clamp(currScale, minSize, maxSize + 1);
        //Sets the target scale to base scale * the new current scale
        targetScale = baseScale * currScale;
    }
}
