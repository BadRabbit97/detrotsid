﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AutoScroll : MonoBehaviour
{
    //*****AUTO SCROLL SCRIPT*****
    //This script controls the text scroll on the ending credit scenes

    //***DECLARED VARIABLES***

    //Public - can see and edit in unity editor
    //Float variable (decimal numeric) - controlls the speed of the scrolling text
    public float scrollSpeed = 100f;
    //Float variable (decimal numeric) - controls when the text begins to scroll
    public float textPosBegin = -1600.0f;
    //Float variable (decimal numeric) - controls where the text ends
    public float boundaryTextEnd = 1538.0f;
    //Float variable (decimal numeric) - controls when the fade out should begin
    public float startFade = 800.0f;
    

    //RectTransform referances - stores the information from the objects rect transform
    RectTransform myGOrectTransform;

    //Game Object referance - stores information on game object
    public GameObject fadeLevel;

    //SerializeField - private but also show up in the Editor
    //GameObject referance - stores all the txt created
    [SerializeField] GameObject mainText;

    //***UPDATES***

    // Start is called before the first frame update
    void Start()
    {
        //Finds the RectTransform on the game object
        myGOrectTransform = gameObject.GetComponent<RectTransform>();
        //Calls AutoScrollText() function
        StartCoroutine(AutoScrollText());
    }

    //***FUNCTIONS***

    //IEnumerator - allows you to iterate through the list of controls
    //This function auto scrolls the text credits
    IEnumerator AutoScrollText()
    {
        //While the game object is lower than the boundery...
        while (myGOrectTransform.localPosition.y < boundaryTextEnd)
        {
            //...scroll text up
            myGOrectTransform.Translate(Vector3.up * scrollSpeed * Time.deltaTime);

            //returns null
            yield return null;
        }

        //If the game object is fully above the boundaries...
        if (myGOrectTransform.localPosition.y > startFade)
        {
            //...load the main menu from scene manager
            //Adds fade to scene change
            fadeLevel.GetComponent<LevelChanger>().FadeToLevel(1);
        }
    }
}
