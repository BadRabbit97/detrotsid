﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyEndMusic : MonoBehaviour
{
    //*****DON'T DETROY END MUSIC*****
    //This script stops the background music from being desroyed

    //Awake is called when the script instance is being loaded
    void Awake()
    {
        //Check how many object in the scene have the tag Music
        GameObject[] objs = GameObject.FindGameObjectsWithTag("EndMusic");
        //If it find more than 1 object...
        if (objs.Length > 1)
        {
            //...destroy the other object
            Destroy(this.gameObject);
        }

        //Stops the object being destroyed
        DontDestroyOnLoad(this.gameObject);
    }
}
