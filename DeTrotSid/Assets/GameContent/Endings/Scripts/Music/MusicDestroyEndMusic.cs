﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicDestroyEndMusic : MonoBehaviour
{
    //*****MUSIC DESTROY END MUSIC****
    //This script stops the background music in the scene

    //Awake is called when the script instance is being loaded
    void Awake()
    {
        //Check how many object in the scene have the tag Music
        GameObject music = GameObject.FindGameObjectWithTag("EndMusic");
        //Destroy the other object
        Destroy(music);
    }
}
